﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Nganhangso2.Models
{
    public class Reports
    {
        public int Id { get; set; }
        [Required, StringLength(50)]
        public string ReportName { get; set; }
        public DateOnly ReportDate { get; set; }

        public int AccountId { get; set; }
        public Accounts? Accounts { get; set; }

        public int LogsId { get; set; }
        public Logs? Logs { get; set; }

        public int TransactionalId { get; set; }
        public Transactions? Transactions { get; set; }


    }
}