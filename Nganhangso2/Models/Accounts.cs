﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Nganhangso2.Models
{
    public class Accounts
    {
        public int Id { get; set; }
        [Required, StringLength(50)]
        public string AccountName { get; set; }
        public int CustomerId { get; set; }
        public Custommer? Custommer { get; set; }
        public List<Reports> Reports { get; set; }
    }
}